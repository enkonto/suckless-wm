/* See LICENSE file for copyright and license details. */

/* appearance */
static const char font[]        = "Ubuntu:size=10:style=Regular:antialias=true:autohint=true";
static const char* normbgcolor  = "#32302f";
static const char* normfgcolor  = "#d4be98";
static const char* selbgcolor   = "#3c3836";
static const char* selfgcolor   = "#89b482";
static const char* urgbgcolor   = "#b47109";
static const char* urgfgcolor   = "#ea6962";
static const char before[]      = "<";
static const char after[]       = ">";
static const char titletrim[]   = "...";
static const int  tabwidth      = 150;
static const Bool foreground    = True;
static       Bool urgentswitch  = False;

/*
 * Where to place a new tab when it is opened. When npisrelative is True,
 * then the current position is changed + newposition. If npisrelative
 * is False, then newposition is an absolute position.
 */
static int  newposition   = 0;
static Bool npisrelative  = True;

#define SETPROP(p) { \
        .v = (char *[]){ "/bin/sh", "-c", \
                "prop=\"`xwininfo -children -id $1 | grep '^     0x' |" \
                "sed -e's@^ *\\(0x[0-9a-f]*\\) \"\\([^\"]*\\)\".*@\\1 \\2@' |" \
                "xargs -0 printf %b | dmenu -l 10 -w $1`\" &&" \
                "xprop -id $1 -f $0 8s -set $0 \"$prop\"", \
                p, winid, NULL \
        } \
}

#define MODKEY ControlMask
static Key keys[] = {
    /* modifier             key             function     argument */
    { MODKEY|Mod1Mask,      XK_t,           focusonce,   { 0 } },
    { MODKEY|Mod1Mask,      XK_t,           spawn,       { 0 } },

    { MODKEY|Mod1Mask,      XK_j,           rotate,      { .i = +1 } },
    { MODKEY|Mod1Mask,      XK_k,           rotate,      { .i = -1 } },
    { MODKEY|Mod1Mask,      XK_h,           movetab,     { .i = -1 } },
    { MODKEY|Mod1Mask,      XK_l,           movetab,     { .i = +1 } },
    { MODKEY|Mod1Mask,      XK_Page_Down,   movetab,     { .i = -1 } },
    { MODKEY|Mod1Mask,      XK_Page_Up,     movetab,     { .i = +1 } },
    { MODKEY|Mod1Mask,      XK_Tab,         rotate,      { .i = 0 } },

    { MODKEY|Mod1Mask,      XK_grave,       spawn,       SETPROP("_TABBED_SELECT_TAB") },
    { MODKEY|Mod1Mask,      XK_1,           move,        { .i = 0 } },
    { MODKEY|Mod1Mask,      XK_2,           move,        { .i = 1 } },
    { MODKEY|Mod1Mask,      XK_3,           move,        { .i = 2 } },
    { MODKEY|Mod1Mask,      XK_4,           move,        { .i = 3 } },
    { MODKEY|Mod1Mask,      XK_5,           move,        { .i = 4 } },
    { MODKEY|Mod1Mask,      XK_6,           move,        { .i = 5 } },
    { MODKEY|Mod1Mask,      XK_7,           move,        { .i = 6 } },
    { MODKEY|Mod1Mask,      XK_8,           move,        { .i = 7 } },
    { MODKEY|Mod1Mask,      XK_9,           move,        { .i = 8 } },
    { MODKEY|Mod1Mask,      XK_0,           move,        { .i = 9 } },

    { MODKEY|Mod1Mask,      XK_q,           killclient,  { 0 } },

    { MODKEY|Mod1Mask,      XK_u,           focusurgent, { 0 } },
    { MODKEY|Mod1Mask,      XK_u,           toggle,      { .v = (void*) &urgentswitch } },

    { MODKEY|Mod1Mask,      XK_F11,         fullscreen,  { 0 } },

    /* Additional key bindings for Programmer Dvorak layout.                       */
    { MODKEY|Mod1Mask,      XK_dollar,      spawn,       SETPROP("_TABBED_SELECT_TAB") },
    { MODKEY|Mod1Mask,      XK_ampersand,   move,        { .i = 0 } },
    { MODKEY|Mod1Mask,      XK_bracketleft, move,        { .i = 1 } },
    { MODKEY|Mod1Mask,      XK_braceleft,   move,        { .i = 2 } },
    { MODKEY|Mod1Mask,      XK_braceright,  move,        { .i = 3 } },
    { MODKEY|Mod1Mask,      XK_parenleft,   move,        { .i = 4 } },
    { MODKEY|Mod1Mask,      XK_equal,       move,        { .i = 5 } },
    { MODKEY|Mod1Mask,      XK_asterisk,    move,        { .i = 6 } },
    { MODKEY|Mod1Mask,      XK_parenright,  move,        { .i = 7 } },
    { MODKEY|Mod1Mask,      XK_plus,        move,        { .i = 8 } },
    { MODKEY|Mod1Mask,      XK_bracketright,move,        { .i = 9 } },

};
