#!/usr/bin/env dash

repo_url="https://git.suckless.org/tabbed"

repo_name="$(basename ${repo_url} .git)"
script_name=$(basename $0)
script_dir="$(dirname $0)"; script_dir="$(pwd)${script_dir#.}"
patch_path="${script_dir}/patches"
func_path="${script_dir%/*}/deploy_functions"
. ${func_path}


_prepairSources


_applyPatch tabbed-clientnumber-20160702-bc23614.diff
cp config.def.h config.h
_applyPatch tabbed-config.h-ab.diff

doas make clean install
