#!/bin/sh

# Example:
# cd modified-program-directory/..
# diff -up original-program-directory modified-program-directory > \
#            toolname-patchname-RELEASE.diff


# ligatures patch have dependency on harfbuzz-devel package (need hb.h)

_applyPatch() {
    printf "\n\n\t%s\n\n" "Applying $*."
    #sleep 2
    patch -p1 < "$patch_path/$*"
    printf "\n\n\t%s\n\n" "$* applied."
}

patch_path="../patches"

rm -fr st
git clone https://git.suckless.org/st
#cp -r st.pure st
cd st

# Renew all sources
#git checkout HEAD config.def.h
#git checkout HEAD config.mk
#git checkout HEAD st.c
#git checkout HEAD st.h
#git checkout HEAD st.1
#git checkout HEAD st.info
#git checkout HEAD x.c
#git checkout HEAD win.h

_applyPatch st-scrollback-20210507-4536f46.diff                    # Successful.
_applyPatch st-scrollback-mouse-20191024-a2c479c.diff              # Successful.
_applyPatch st-scrollback-mouse-altscreen-20200416-5703aa0.diff    # Successful.
_applyPatch st-anysize-0.8.4.diff                                  # Successful.
_applyPatch st-appsync-20200618-b27a383.diff                       # Successful.
_applyPatch st-blinking_cursor-20200531-a2a7044.diff               # Successful.
_applyPatch st-blinking_cursor-20200531-a2a7044-ab.diff            # Successful.
# TODO: Removed 'bold is not bright' patch. Can be issues.
_applyPatch st-delkey-20201112-4ef0cbd-ab.diff                     # Successful.
_applyPatch st-ligatures-scrollback-20210824-0.8.4.diff            # Successful.
# TODO: Removed 'gruvebox dark theme'. Can be issues. Make gruvbox material dark
#       theme patch.
_applyPatch st-vertcenter-20180320-6ac8c8a.diff                    # Successful.
_applyPatch st-visualbell-0.8.4.diff                               # Successful.
_applyPatch st-visualbell-0.8.4-ab.diff                            # Successful.
# Confirm backwards patching. TODO: patch work wrong.
#_applyPatch fix_bold_0.8.2-github_com_Lense.diff                   # Successful.
_applyPatch gruvbox-dark-theme.diff                                # Successful.
cp config.def.h config.h
_applyPatch st-config.h-ab.diff

doas make clean install
