#!/bin/sh

# Example:
# cd modified-program-directory/..
# diff -up original-program-directory modified-program-directory > \
#            toolname-patchname-RELEASE.diff


_applyPatch() {
    printf "\n\n\t%s\n\n" "Applying $*."
    #sleep 2
    patch -p1 < "$patch_path/$*"
    printf "\n\n\t%s\n\n" "$* applied."
}

patch_path="../patches"

rm -fr dwm
#git clone https://git.suckless.org/dwm
cp -r dwm.pure dwm
cd dwm

# Renew all sources
#git checkout HEAD config.def.h
#git checkout HEAD config.mk
#git checkout HEAD drw.c
#git checkout HEAD drw.h
#git checkout HEAD dwm.1
#git checkout HEAD dwm.c
#git checkout HEAD transient.c
#git checkout HEAD util.c
#git checkout HEAD util.h

# Because of heavely patched config.def.h by kaychain patch should be applied at
# the very begining.
_applyPatch dwm-keychain-20200729-053e3a2.diff                  # Successful.
_applyPatch accessnthmon.diff                                   # Successful.
_applyPatch accessnthmon-ab.diff                                # Successful.
# TODO: Excluded actualfullscreen. Can be issues.
_applyPatch dwm-alwayscenter-20200625-f04cac6.diff              # Successful.
_applyPatch dwm-focusmaster-return-6.2.diff                     # Successful.
_applyPatch dwm-focusmaster-return-6.2-ab.diff                  # Successful.
_applyPatch dwm-movestack-6.1.diff                              # Successful.
_applyPatch dwm-movestack-6.1-ab.diff                           # Successful.
_applyPatch dwm-pertag-20200914-61bb8b2.diff                    # Successful.
_applyPatch dwm-resizecorners-6.2.diff                          # Successful.
_applyPatch dwm-rmaster-6.2.diff                                # Successful.
_applyPatch dwm-rmaster-6.2-ab.diff                             # Successful.
_applyPatch dwm-savefloats-20181212-b69c870.diff                # Successful.
_applyPatch dwm-savefloats-20181212-b69c870-ab.diff             # Successful.
_applyPatch dwm-scratchpad-6.2.diff                             # Successful.
_applyPatch dwm-scratchpad-6.2-ab.diff                          # Successful.
_applyPatch dwm-smartborders-6.2.diff                           # Successful.
# TODO: Fix bugs. Border sometimes do not appear in WS with multiple windows.
_applyPatch dwm-smartborders-6.2-ab-resizecorners-rmaster-savefloats.diff # Successful.
_applyPatch dwm-systray-20210418-67d76bd.diff                   # Successful.
_applyPatch dwm-systray-20210418-67d76bd-ab-smartborders.diff   # Successful.
_applyPatch dwm-warp-6.2.diff                                   # Successful.
# Zoomswap issues warnings.
_applyPatch dwm-zoomswap-6.2.diff                               # Successful.
_applyPatch dwm-zoomswap-6.2-ab.diff                            # Successful.
# Additional layouts.
_applyPatch dwm-horizgrid-6.1.diff                              # Successful.
_applyPatch dwm-horizgrid-6.1-ab-smartborders.diff              # Successful.
_applyPatch dwm-bottomstack-6.1.diff                            # Successful.
_applyPatch dwm-bottomstack-6.1-ab-smartborders.diff            # Successful.
_applyPatch dwm-deck-double-smartborders-6.2.diff               # Successful.
_applyPatch dwm-deck-double-smartborders-6.2-ab.diff            # Successful.
_applyPatch dwm-cfacts-20200913-61bb8b2.diff                    # Successful.
_applyPatch dwm-cfacts-20200913-61bb8b2-ab.diff                 # Successful.
# Custom cfacts layout patches should be applied instead geniune patch.
_applyPatch dwm-cfacts_bottomstack-6.2-ab-smartborders.diff     # Successful.
cp config.def.h config.h
_applyPatch dwm-config.h-ab.diff

doas make clean install
