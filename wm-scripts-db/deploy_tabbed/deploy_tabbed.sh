#!/usr/bin/env dash

# Example:
# cd modified-program-directory/..
# diff -up original-program-directory modified-program-directory > \
#            toolname-patchname-RELEASE.diff


_applyPatch() {
    printf "\n\n\t%s\n\n" "Applying $*."
    #sleep 2
    patch -p1 < "$patch_path/$*"
    printf "\n\n\t%s\n\n" "$* applied."
}

patch_path="../patches"

rm -fr tabbed
git clone https://git.suckless.org/tabbed
cd tabbed

_applyPatch tabbed-clientnumber-20160702-bc23614.diff
cp config.def.h config.h
_applyPatch tabbed-config.h-ab.diff

doas make clean install
