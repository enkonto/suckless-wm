#!/usr/bin/env dash

# Example:
# cd modified-program-directory/..
# diff -up original-program-directory modified-program-directory > \
#            toolname-patchname-RELEASE.diff


_applyPatch() {
    printf "\n\n\t%s\n\n" "Applying $*."
    #sleep 2
    patch -p1 < "$patch_path/$*"
    printf "\n\n\t%s\n\n" "$* applied."
}

patch_path="../patches"

rm -fr dwmblocks
git clone https://github.com/torrinfail/dwmblocks.git
cd dwmblocks
cp blocks.def.h blocks.h
_applyPatch dwmblocks-blocks.h-ab.diff

doas make clean install
