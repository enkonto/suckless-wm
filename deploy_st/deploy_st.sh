#!/usr/bin/env dash

repo_url="https://git.suckless.org/st"

repo_name="$(basename ${repo_url} .git)"
script_name=$(basename $0)
script_dir="$(dirname $0)"; script_dir="$(pwd)${script_dir#.}"
patch_path="${script_dir}/patches"
func_path="${script_dir%/*}/deploy_functions"
. ${func_path}


_prepairSources
# Renew all sources
#git checkout HEAD config.def.h
#git checkout HEAD config.mk
#git checkout HEAD st.c
#git checkout HEAD st.h
#git checkout HEAD st.1
#git checkout HEAD st.info
#git checkout HEAD x.c
#git checkout HEAD win.h

_applyPatch st-scrollback-20210507-4536f46.diff                 # .
_applyPatch st-scrollback-mouse-20191024-a2c479c.diff           # .
_applyPatch st-scrollback-mouse-altscreen-20200416-5703aa0.diff # .
_applyPatch st-anysize-0.8.4.diff                               # .
_applyPatch st-appsync-20200618-b27a383.diff                    # .
_applyPatch st-blinking_cursor-20200531-a2a7044.diff            # .
_applyPatch st-blinking_cursor-20200531-a2a7044-ab.diff         # .
_applyPatch st-delkey-20201112-4ef0cbd-ab.diff                  # .
# Some times don't work.
#_applyPatch st-dynamic-cursor-color-0.9.diff
# Patch for ligatures support depend on ``harfbuzz-devel'' package.
_applyPatch st-ligatures-scrollback-20210824-0.8.4.diff         # .
_applyPatch st-ligatures-scrollback-20210824-0.8.4-ab.diff         # .
#_applyPatch st-undercurl-0.8.4-20210822.diff
_applyPatch st-vertcenter-20180320-6ac8c8a.diff                 # .
_applyPatch st-vertcenter-20180320-6ac8c8a-ab.diff                 # .
_applyPatch st-visualbell-0.8.4.diff                            # .
_applyPatch st-visualbell-0.8.4-ab.diff                         # .

# Choose color theme.
#_applyPatch gruvbox-dark-material.diff                          # .
#_applyPatch gruvbox-dark-mix.diff                               # .
_applyPatch gruvbox-dark-original.diff                          # .

cp config.def.h config.h
_applyPatch st-config.h-ab.diff

doas make clean install
