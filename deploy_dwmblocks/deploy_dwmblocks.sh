#!/usr/bin/env dash

repo_url="https://git.suckless.org/dwmblocks"

repo_name="$(basename ${repo_url} .git)"
script_name=$(basename $0)
script_dir="$(dirname $0)"; script_dir="$(pwd)${script_dir#.}"
patch_path="${script_dir}/patches"
func_path="${script_dir%/*}/deploy_functions"
. ${func_path}


_prepairSources


cp blocks.def.h blocks.h
_applyPatch dwmblocks-blocks.h-ab.diff

doas make clean install
