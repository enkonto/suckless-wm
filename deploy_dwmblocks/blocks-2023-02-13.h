//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
    /*Icon*/    /*Command*/                 /*Update Interval*/    /*Update Signal*/

/*
    {"",        "blocks_packages.sh",       43200,                  0},
    {"",        "blocks_mail.sh",           14400,                  0},
    {"",        "blocks_weather.sh",        900,                    0},
*/
    {"",        "blocks_battery.sh",        300,                    6},
    {"",        "blocks_backlight.sh",      0,                      5},
    {"",        "blocks_load.sh",           15,                     0},
    {"",        "blocks_diskspace.sh",      1800,                   0},
/*
    {"",        "blocks_network.sh",        15,                     4},
    {"",        "blocks_usb.sh",            0,                      3},
*/
    {"",        "blocks_volume.sh",         0,                      2},
    {"",        "blocks_layout.sh",         0,                      1},
    {"",        "blocks_clock.sh",          15,                     0},
    {"",        "blocks_pomodoro.sh",       60,                     10},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = " ";
static unsigned int delimLen = 5;
