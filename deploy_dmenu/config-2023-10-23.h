/* See LICENSE file for copyright and license details. */
/* Default settings; can be overriden by command line. */

static int topbar = 1;          /* -b  option; if 0, dmenu appears at bottom */
static int fuzzy = 1;           /* -F  option; if 0, dmenu doesn't use fuzzy matching */
static int centered = 0;        /* -c option; centers dmenu on screen */
static int min_width = 1200;    /* minimum width when centered */
/* -fn option overrides fonts[0]; default X11 font or font set */
static const char *fonts[] = {
    "JetBrains Mono:style=Regular:size=11"
};
static const char *prompt      = NULL;      /* -p  option; prompt to the left of input field */
static const char *colors[SchemeLast][2] = {
                        /*     fg         bg       */
    [SchemeNorm] =          { "#fabd2f", "#282828" },
    [SchemeSel]  =          { "#1d2021", "#a89984" },
    [SchemeSelHighlight]  = { "#cc241d", "#a89984" },
    [SchemeNormHighlight] = { "#fb4934", "#282828" },
    [SchemeOut]  =          { "#1d2021", "#d3869b" },
    /*
    [SchemeNorm] =          { "#1d2021", "#a89984" },
    [SchemeSel]  =          { "#fabd2f", "#282828" },
    [SchemeSelHighlight]  = { "#fb4934", "#282828" },
    [SchemeNormHighlight] = { "#cc241d", "#a89984" },
    [SchemeOut]  =          { "#1d2021", "#d3869b" },

    [SchemeNorm] =          { "#fabd2f", "#282828" },
    [SchemeSel]  =          { "#1d2021", "#a89984" },
    [SchemeSelHighlight]  = { "#cc241d", "#a89984" },
    [SchemeNormHighlight] = { "#fb4934", "#282828" },
    [SchemeOut]  =          { "#1d2021", "#d3869b" },

    [SchemeNorm] =          { "#D8A657", "#32302F" },
    [SchemeSel]  =          { "#D4BE98", "#4E4E4E" },
    [SchemeSelHighlight]  = { "#EA6962", "#4E4E4E" },
    [SchemeNormHighlight] = { "#C14A4A", "#32302F" },
    [SchemeOut]  =          { "#EA6962", "#D3869B" },
     */
};
/* -l and -g options; controls number of lines and columns in grid if > 0 */
static unsigned int lines      = 0;
static unsigned int columns    = 2;
/* -h option; minimum height of a menu line */
static unsigned int lineheight = 30;
static unsigned int min_lineheight = 8;

/*
 * Characters not considered part of a word while deleting words
 * for example: " /?\"&[]"
 */
static const char worddelimiters[] = " ";

/* Size of the window border */
static unsigned int border_width = 0;
