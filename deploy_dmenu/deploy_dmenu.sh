#!/usr/bin/env dash

repo_url="https://git.suckless.org/dmenu"

repo_name="$(basename ${repo_url} .git)"
script_name=$(basename $0)
script_dir="$(dirname $0)"; script_dir="$(pwd)${script_dir#.}"
patch_path="${script_dir}/patches"
func_path="${script_dir%/*}/deploy_functions"
. ${func_path}


_prepairSources


_applyPatch dmenu-border-20201112-1a13d04.diff                  # Successful.
_applyPatch dmenu-caseinsensitive-5.0.diff                      # Successful.
_applyPatch dmenu-center-20200111-8cd37e1.diff                  # Successful.
_applyPatch dmenu-center-20200111-8cd37e1-ab.diff               # Successful.
_applyPatch dmenu-fuzzymatch-4.9.diff                           # Successful.
_applyPatch dmenu-fuzzymatch-4.9-ab.diff                        # Successful.
_applyPatch dmenu-fuzzyhighlight-4.9.diff                       # Successful.
_applyPatch dmenu-grid-4.9.diff                                 # Successful.
_applyPatch dmenu-grid-4.9-ab.diff                              # Successful.
_applyPatch dmenu-gridnav-5.0.diff                              # Successful.
_applyPatch dmenu-gridnav-5.0-ab.diff                           # Successful.
_applyPatch dmenu-lineheight-5.0.diff                           # Successful.
_applyPatch dmenu-lineheight-5.0-ab.diff                        # Successful.
_applyPatch dmenu-linesbelowprompt-20210703-1a13d04-ab.diff     # Successful.
_applyPatch dmenu-numbers-4.9.diff                              # Successful.
_applyPatch dmenu-password-5.0.diff                             # Successful.
_applyPatch dmenu-password-5.0-ab.diff                          # Successful.
_applyPatch dmenu-managed-4.9.diff                              # Successful.
_applyPatch dmenu-managed-4.9-ab.diff                           # Successful.
cp config.def.h config.h
_applyPatch dmenu-config.h-ab.diff

doas make clean install
