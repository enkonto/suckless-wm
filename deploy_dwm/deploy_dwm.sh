#!/usr/bin/env dash

repo_url="https://git.suckless.org/dwm"

repo_name="$(basename ${repo_url} .git)"
script_name=$(basename $0)
script_dir="$(dirname $0)"; script_dir="$(pwd)${script_dir#.}"
patch_path="${script_dir}/patches"
func_path="${script_dir%/*}/deploy_functions"
. ${func_path}


_prepairSources
# Renew all sources
#git checkout HEAD config.def.h
#git checkout HEAD config.mk
#git checkout HEAD drw.c
#git checkout HEAD drw.h
#git checkout HEAD dwm.1
#git checkout HEAD dwm.c
#git checkout HEAD transient.c
#git checkout HEAD util.c
#git checkout HEAD util.h


# Because of heavely patched config.def.h by kaychain patch should be applied at
# the very begining.
_applyPatch dwm-keychain-20200729-053e3a2.diff                  # Successful.
_applyPatch accessnthmon.diff                                   # Successful.
_applyPatch accessnthmon-ab.diff                                # Successful.
_applyPatch dwm-alwayscenter-20200625-f04cac6.diff              # Successful.
_applyPatch dwm-movestack-6.1.diff                              # Successful.
_applyPatch dwm-movestack-6.1-ab.diff                           # Successful.
_applyPatch dwm-pertag-20200914-61bb8b2.diff                    # Successful.
_applyPatch dwm-resizecorners-6.2.diff                          # Successful.
_applyPatch dwm-rmaster-6.2.diff                                # Successful.
_applyPatch dwm-rmaster-6.2-ab.diff                             # Successful.
_applyPatch dwm-savefloats-20181212-b69c870.diff                # Successful.
_applyPatch dwm-savefloats-20181212-b69c870-ab.diff             # Successful.
_applyPatch dwm-scratchpad-6.2.diff                             # Successful.
_applyPatch dwm-scratchpad-6.2-ab.diff                          # Successful.
_applyPatch dwm-smartborders-6.2.diff                           # Successful.
# Fixed borders which sometimes do not appear in WS with multiple windows.
_applyPatch dwm-smartborders-6.2-ab-resizecorners-rmaster-savefloats.diff # Successful.
_applyPatch dwm-systray-6.3.diff                                # Successful.
_applyPatch dwm-systray-6.3-ab-smartborders.diff                # Successful.
_applyPatch dwm-warp-6.2.diff                                   # Successful.
_applyPatch dwm-zoomswap-6.2.diff                               # Successful.
_applyPatch dwm-zoomswap-6.2-ab.diff                            # Successful.
# Additional layouts.
_applyPatch dwm-horizgrid-6.1.diff                              # Successful.
_applyPatch dwm-horizgrid-6.1-ab-smartborders.diff              # Successful.
_applyPatch dwm-deck-double-smartborders-6.2.diff               # Successful.
_applyPatch dwm-deck-double-smartborders-6.2-ab.diff            # Successful.
_applyPatch dwm-cfacts-20200913-61bb8b2.diff                    # Successful.
_applyPatch dwm-cfacts-20200913-61bb8b2-ab.diff                 # Successful.
cp config.def.h config.h
_applyPatch dwm-config.h-ab.diff

uberuser make clean install
