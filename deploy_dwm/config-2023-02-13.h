/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx      = 3;    /* border pixel of windows */
static const unsigned int snap          = 32;   /* snap pixel */
static const int rmaster                = 0;    /* 1 means master-area is
                                                 * initially on the right */
static const unsigned int systraypinning = 0;   /* 0: sloppy systray follows
                                                 * selected monitor, >0:
                                                 * pin systray to monitor X */
static const unsigned int systrayonleft  = 0;   /* 0: systray in the right
                                                 * corner, >0: systray on left
                                                 * of status text */
static const unsigned int systrayspacing = 2;   /* systray spacing */
static const int systraypinningfailfirst = 1;   /* 1: if pinning fails, display
                                                 * systray on the first monitor,
                                                 * False: display systray on the
                                                 * last monitor */
static const int showsystray            = 1;    /* 0 means no systray */
static const int showbar                = 1;    /* 0 means no bar */
static const int topbar                 = 1;    /* 0 means bottom bar */
static const char *fonts[]              = { "FiraGO:size=12" };
static const char dmenufont[]           = "JetBrains Mono:size=10";
static const char col_bg_normal[]       = "#32302F";    /* bg normal */
static const char col_bg_focus[]        = "#32302F";    /* bg focus */
static const char col_fg_normal[]       = "#7DAEA3";    /* fg normal */
static const char col_fg_focus[]        = "#A9B665";    /* fg focus */
static const char col_border_normal[]   = "#3C3836";    /* border normal */
static const char col_border_focus[]    = "#D8A657";    /* border focus */
static const char *colors[][3]          = {
    /*                  fg              bg              border      */
    [SchemeNorm] = { col_fg_normal, col_bg_normal, col_border_normal },
    [SchemeSel]  = { col_fg_focus,  col_bg_focus,  col_border_focus  },
};

/* tagging */
static const char *tags[] = { "", "", "", "", "", "", "", "", "" };

static const Rule rules[] = {
    /* xprop(1):
     *  WM_CLASS(STRING) = instance, class
     *  WM_NAME(STRING) = title
     *
     * class            instance        title  tags mask isfloating monitor */
    { "kitty",          NULL,           NULL,   1 << 0,     0,      -1 },
    { "st",             NULL,           NULL,   1 << 0,     0,      -1 },
    { "tabbed",         "st",           NULL,   1 << 0,     0,      -1 },
    { "URxvt",          NULL,           NULL,   1 << 0,     0,      -1 },
    { "tabbed",         "URxvt",        NULL,   1 << 0,     0,      -1 },
    { "Alacritty",      NULL,           NULL,   1 << 0,     0,      -1 },
    { "tabbed",         "Alacritty",    NULL,   1 << 0,     0,      -1 },
    { "Firefox",        NULL,           NULL,   1 << 1,     0,      -1 },
    { "Chromium",       NULL,           NULL,   1 << 1,     0,      -1 },
    { "qutebrowser",    NULL,           NULL,   1 << 1,     0,      -1 },
    { "Zathura",        NULL,           NULL,   1 << 2,     0,      -1 },
    { "Gimp",           "gimp",         NULL,   1 << 5,     0,      -1 },
    { "Sxiv",           "sxiv",         NULL,   1 << 5,     0,      -1 },
    { "MPlayer",        NULL,           NULL,   1 << 6,     1,      -1 },
    { "mpv",            NULL,           NULL,   1 << 6,     1,      -1 },
    { "vlc",            NULL,           NULL,   1 << 6,     1,      -1 },
    { "Deadbeef",       "deadbeef",     NULL,   1 << 7,     0,      -1 },
    { "Emacs",          "emacs",        NULL,   1 << 8,     0,      -1 },
    { "my_float_window",NULL,           NULL,   1 << 0,     1,      -1 },
};

/* layout(s) */
static const float mfact        = 0.5;  /* factor of master area size [0.05..0.95] */
static const int nmaster        = 1;    /* number of clients in master area */
static const int resizehints    = 1;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1;    /* 1 will force focus on the fullscreen window */

#include "horizgrid.c"
static const Layout layouts[] = {
    /* symbol   arrange function */
    { "[]=",    tile }, /* first entry is default */
    { "><>",    NULL }, /* no layout function means floating behavior */
    { "[M]",    monocle },
    { "###",    horizgrid },
    { "D|D",    doubledeck },
};

/* key definitions */
#define MODKEY Mod3Mask
#define ALTMOD Mod4Mask
#define TAGKEYS(CHAIN,KEY,TAG) \
    { MODKEY,                       CHAIN,  KEY,    view,           {.ui = 1 << TAG} }, \
    { MODKEY|ControlMask,           CHAIN,  KEY,    toggleview,     {.ui = 1 << TAG} }, \
    { MODKEY|ShiftMask,             CHAIN,  KEY,    tag,            {.ui = 1 << TAG} }, \
    { MODKEY|ControlMask|ShiftMask, CHAIN,  KEY,    toggletag,      {.ui = 1 << TAG} }, \
        { ALTMOD,                   CHAIN,  KEY,    focusnthmon,    {.i  = TAG } }, \
        { ALTMOD|ShiftMask,         CHAIN,  KEY,    tagnthmon,      {.i  = TAG } },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/dash", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
/*
 * static const char *dmenucmd[]       = { "dmenu_run", "-m", dmenumon, "-fn",
 *                                         dmenufont, NULL };
 *
 * static const char *termcmd[]        = { "urxvt256cc", NULL };
 * static const char *tabtermcmd[]     = { "tabbed", "-c", "-k", "-p", "s+1000",
 *                                         "-n", "URxvt", "-r", "2", "urxvt256cc",
 *                                         "-embed", "''", NULL };
 * static const char *scratchpadcmd[]  = { "urxvt256cc", "-name", scratchpadname,
 *                                         "-geometry", "100x28", NULL };
 */
static const char scratchpadname[]  = "scratchpad";
static const char *dmenucmd[]       = { "dmenu_run", "-m", dmenumon, "-fn",
                                        dmenufont, "-nb", col_bg_normal,
                                        "-nf", col_fg_normal, "-sb",
                                        col_bg_focus, "-sf", col_fg_focus,
                                        NULL };
static const char *passcmd[]        = { "keepmenu", NULL };
static const char *emacscmd[]       = { "emacsclient", "-c", "-a", "emacs", NULL };

static const char *termcmd[]        = { "st", NULL };
static const char *tabtermcmd[]     = { "tabbed", "-c", "-k", "-p", "s+1000",
                                        "-n", "st", "-r", "2", "st", "-w", "''", NULL };
static const char *scratchpadcmd[]  = { "st", "-t", scratchpadname, "-c",
                                        "my_float_window", "-g", "100x28", NULL };

#include "movestack.c"
#include <X11/XF86keysym.h>
static Key keys[] = {
    /* modifier           chain key     key           function        argument */
    { MODKEY,             -1,           XK_p,         spawn,          {.v = dmenucmd } },
    { MODKEY|ControlMask, -1,           XK_p,         spawn,          {.v = passcmd } },
    { MODKEY,             -1,           XK_Return,    spawn,          {.v = termcmd } },
    { MODKEY|ControlMask, -1,           XK_Return,    spawn,          {.v = tabtermcmd } },
    { MODKEY|Mod1Mask,    -1,           XK_Return,    spawn,          {.v = emacscmd } },
    { MODKEY,             -1,           XK_grave,     togglescratch,  {.v = scratchpadcmd } },
    { MODKEY,             -1,           XK_b,         togglebar,      {0} },
    { MODKEY,             -1,           XK_j,         focusstack,     {.i = +1 } },
    { MODKEY,             -1,           XK_k,         focusstack,     {.i = -1 } },
    { MODKEY,             -1,           XK_i,         incnmaster,     {.i = +1 } },
    { MODKEY,             -1,           XK_d,         incnmaster,     {.i = -1 } },
    { MODKEY,             -1,           XK_h,         setmfact,       {.f = -0.10} },
    { MODKEY,             -1,           XK_l,         setmfact,       {.f = +0.10} },
    { MODKEY|ControlMask, -1,           XK_h,         setcfact,       {.f = -0.25} },
    { MODKEY|ControlMask, -1,           XK_l,         setcfact,       {.f = +0.25} },
    { MODKEY|ControlMask, -1,           XK_e,         setcfact,       {.f =  0.0 } },
    { MODKEY|ControlMask, -1,           XK_j,         movestack,      {.i = +1 } },
    { MODKEY|ControlMask, -1,           XK_k,         movestack,      {.i = -1 } },
    { MODKEY,             -1,           XK_z,         zoom,           {0} },
    { MODKEY,             -1,           XK_Tab,       view,           {0} },
    { MODKEY,             -1,           XK_BackSpace, killclient,     {0} },
    { MODKEY,             -1,           XK_s,         setlayout,      {.v = &layouts[0]} }, /* "[]=", stack       */
    { MODKEY,             -1,           XK_f,         setlayout,      {.v = &layouts[1]} }, /* "><>", floating    */
    { MODKEY,             -1,           XK_m,         setlayout,      {.v = &layouts[2]} }, /* "[M]", monocle     */
    { MODKEY,             -1,           XK_g,         setlayout,      {.v = &layouts[3]} }, /* "###", grid        */
    { MODKEY,             -1,           XK_c,         setlayout,      {.v = &layouts[4]} }, /* "D|D", doubledeck  */
    { MODKEY|Mod1Mask,    -1,           XK_space,     setlayout,      {0} },
    { MODKEY,             -1,           XK_space,     togglefloating, {0} },
    { MODKEY,             -1,           XK_r,         togglermaster,  {0} },
    { MODKEY,             -1,           XK_0,         view,           {.ui = ~0 } },
    { MODKEY|ControlMask, -1,           XK_0,         tag,            {.ui = ~0 } }, /* Stick current window. */
    { MODKEY,             -1,           XK_comma,     focusmon,       {.i = -1 } },
    { MODKEY,             -1,           XK_period,    focusmon,       {.i = +1 } },
    { MODKEY|ControlMask, -1,           XK_comma,     tagmon,         {.i = -1 } },
    { MODKEY|ControlMask, -1,           XK_period,    tagmon,         {.i = +1 } },
    TAGKEYS(              -1,           XK_1,                         0)
    TAGKEYS(              -1,           XK_2,                         1)
    TAGKEYS(              -1,           XK_3,                         2)
    TAGKEYS(              -1,           XK_4,                         3)
    TAGKEYS(              -1,           XK_5,                         4)
    TAGKEYS(              -1,           XK_6,                         5)
    TAGKEYS(              -1,           XK_7,                         6)
    TAGKEYS(              -1,           XK_8,                         7)
    TAGKEYS(              -1,           XK_9,                         8)
    { MODKEY|Mod1Mask,    -1,           XK_Escape,    quit,           {0} },
    /* Remove some bindings to exclude double action of `dwm' and `sxhkd'. */
    { MODKEY,             -1,           XK_F1,        spawn,          SHCMD("eval $EMBTERM sl_hotkeys.sh") },
    { MODKEY,             -1,           XK_apostrophe,spawn,          SHCMD("dmenu_launcher.sh") },
    { MODKEY|ControlMask, -1,           XK_Escape,    spawn,          SHCMD("dmenu_system.sh") },
    { MODKEY,             XK_q,         XK_h,         spawn,          SHCMD("doas shutdown -h now") },
    { MODKEY,             XK_q,         XK_r,         spawn,          SHCMD("doas shutdown -r now") },
    { MODKEY,             XK_q,         XK_z,         spawn,          SHCMD("doas zzz") },
    { MODKEY,             XK_q,         XK_Z,         spawn,          SHCMD("doas ZZZ") },
    { MODKEY,             XK_q,         XK_l,         spawn,          SHCMD("xseclock.sh") },
    { MODKEY,             -1,           XK_F6,        spawn,          SHCMD("display_menu.sh") },
    { MODKEY,             -1,           XK_F7,        spawn,          SHCMD("init_pointdevs.sh toggle") },
    { MODKEY,             -1,           XK_F8,        spawn,          SHCMD("~/.onresume") },
    /* Use `sleep' to prevent flaws with `scrot' or force to run `scrot' on key
     * release if `dwm' patched with according patch. */
    { Mod1Mask, -1, XK_Print,spawn,SHCMD("sleep 0.2; scrot -s -l color='#91231C' -e 'mv $f ~/shots/'") },
    { ShiftMask,-1, XK_Print,spawn,SHCMD("sleep 0.2; scrot -u -e 'mv $f ~/shots/'") },
    { 0,        -1, XF86XK_Display,                   spawn,          SHCMD("~/.onresume") },
    { 0,        -1, XF86XK_AudioPlay,                 spawn,          SHCMD("deadbeef --play-pause") },
    { 0,        -1, XF86XK_AudioStop,                 spawn,          SHCMD("deadbeef --stop") },
    { 0,        -1, XF86XK_AudioPrev,                 spawn,          SHCMD("deadbeef --prev") },
    { 0,        -1, XF86XK_AudioNext,                 spawn,          SHCMD("deadbeef --next") },
    { 0,        -1, XF86XK_AudioMute,                 spawn,          SHCMD("pkill -RTMIN+2 dwmblocks") },
    { 0,        -1, XF86XK_AudioMicMute,              spawn,          SHCMD("pkill -RTMIN+2 dwmblocks") },
    { 0,        -1, XF86XK_AudioRaiseVolume,          spawn,          SHCMD("pkill -RTMIN+2 dwmblocks") },
    { 0,        -1, XF86XK_AudioLowerVolume,          spawn,          SHCMD("pkill -RTMIN+2 dwmblocks") },
    { ShiftMask,-1, XK_Alt_R,                         spawn,          SHCMD("pkill -RTMIN+1 dwmblocks") },
    { ShiftMask,-1, XK_Alt_L,                         spawn,          SHCMD("pkill -RTMIN+1 dwmblocks") },

    /* Additional key bindings for Programmer Dvorak layout.                       */
    { MODKEY,             -1, XK_dollar,              togglescratch, {.v = scratchpadcmd } },
    { MODKEY,             -1, XK_bracketright,        view,          {.ui = ~0 } },
    { MODKEY|ControlMask, -1, XK_bracketright,        tag,           {.ui = ~0 } },
    TAGKEYS(              -1, XK_ampersand,                          0)
    TAGKEYS(              -1, XK_bracketleft,                        1)
    TAGKEYS(              -1, XK_braceleft,                          2)
    TAGKEYS(              -1, XK_braceright,                         3)
    TAGKEYS(              -1, XK_parenleft,                          4)
    TAGKEYS(              -1, XK_equal,                              5)
    TAGKEYS(              -1, XK_asterisk,                           6)
    TAGKEYS(              -1, XK_parenright,                         7)
    TAGKEYS(              -1, XK_plus,                               8)
    { MODKEY|Mod1Mask,    -1, XK_dollar,              spawn,         SHCMD("dmenu_launcher.sh") },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
    /* click            event mask  button      function        argument */
    { ClkLtSymbol,      0,          Button1,    setlayout,      {0} },
    { ClkLtSymbol,      0,          Button3,    setlayout,      {.v = &layouts[2]} },
    { ClkWinTitle,      0,          Button2,    zoom,           {0} },
    { ClkStatusText,    0,          Button2,    spawn,          {.v = termcmd } },
    { ClkClientWin,     MODKEY,     Button1,    movemouse,      {0} },
    { ClkClientWin,     MODKEY,     Button2,    togglefloating, {0} },
    { ClkClientWin,     MODKEY,     Button3,    resizemouse,    {0} },
    { ClkTagBar,        0,          Button1,    view,           {0} },
    { ClkTagBar,        0,          Button3,    toggleview,     {0} },
    { ClkTagBar,        MODKEY,     Button1,    tag,            {0} },
    { ClkTagBar,        MODKEY,     Button3,    toggletag,      {0} },
};
